# DO NOT Modify this file unless you are changing the project default!
#
# For your local changes, use a env.{dev,prod,test}.sh file to override what you
# need.

export SECRET_KEY_BASE='hd+n7dK6335YDLdOlZJ7Mc1opCXCyUONo7LN4wjEOsfw8CedkX1KU7h/JacKDdV+'
export ERLANG_COOKIE='omnomnomnomnom'
export PORT=3000
export URL_HOST=localhost
export URL_SCHEME=http
export URL_PORT=3000
export REPLACE_OS_VARS=true
export NODE_NAME=project-saturn
export MAILGUN_API_KEY="fake_api_key"
export MAILGUN_DOMAIN="mg.40liters.com"
export BASIC_AUTH_USERNAME=admin
export BASIC_AUTH_PASSWORD=password
export MAILGUN_BASE_URI="https://api.eu.mailgun.net/v3"
export PROJECT_SATURN_EMAIL_SITE_NAME="Saturn Project"
export PROJECT_SATURN_EMAIL_FROM_NAME="Io"
export PROJECT_SATURN_EMAIL_FROM="contact@40liters.com"
export PROJECT_SATURN_UPLOAD_DIR=$(pwd)/uploads

if test -f "env.local.sh"; then
	. "env.local.sh"
fi
