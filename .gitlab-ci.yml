stages:
  - build
  - test
  - release
  - tag image
  - deploy to production

variables:
  # By default we don't checkout the code
  GIT_STRATEGY: none

Build the builder:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug-edge
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  before_script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  script:
    - export APP_NAME=`grep 'app:' mix.exs | sed -e 's/\[//g' -e 's/ //g' -e 's/app://' -e 's/[:,]//g'`
    - export APP_VSN=`grep 'version:' mix.exs | cut -d '"' -f2`
    - export BUILD="${CI_COMMIT_SHORT_SHA}"
    - export MIX_ENV='prod'
    - /kaniko/executor --context="${CI_PROJECT_DIR}"
      --cache
      --dockerfile="${CI_PROJECT_DIR}/Dockerfile"
      --target="builder"
      --build-arg APP_NAME="${APP_NAME}"
      --build-arg APP_VSN="${APP_VSN}"
      --destination="${CI_REGISTRY_IMAGE}:builder-${CI_COMMIT_SHORT_SHA}"
      --destination="${CI_REGISTRY_IMAGE}:builder-${CI_COMMIT_REF_SLUG}"
      --destination="${CI_REGISTRY_IMAGE}:builder"
  except:
    - master

Build the tester:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug-edge
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  before_script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  script:
    - export APP_NAME=`grep 'app:' mix.exs | sed -e 's/\[//g' -e 's/ //g' -e 's/app://' -e 's/[:,]//g'`
    - export APP_VSN=`grep 'version:' mix.exs | cut -d '"' -f2`
    - export BUILD="${CI_COMMIT_SHORT_SHA}"
    - export MIX_ENV='prod'
    - /kaniko/executor --context="${CI_PROJECT_DIR}"
      --cache
      --target="builder"
      --dockerfile="${CI_PROJECT_DIR}/Dockerfile"
      --build-arg APP_NAME="${APP_NAME}"
      --build-arg APP_VSN="${APP_VSN}"
      --build-arg MIX_ENV="test"
      --destination="${CI_REGISTRY_IMAGE}:tester-${CI_COMMIT_SHORT_SHA}"
      --destination="${CI_REGISTRY_IMAGE}:tester-${CI_COMMIT_REF_SLUG}"
      --destination="${CI_REGISTRY_IMAGE}:tester"
  except:
    - master

Test:
  stage: test
  image: $CI_REGISTRY_IMAGE:tester-$CI_COMMIT_SHORT_SHA
  variables:
    GIT_STRATEGY: clone
    MIX_ENV: test
  script:
    # GitLab change the path to the one specific to the build. Which we don't want.
    # A more generic way to handle that would be to have APP_PATH env variable in the image maybe?
    # Also, the tests are super slow because mix rebuild everything in test mode.
    # Could be worth it separate the runtime + build (old build), and test image.
    - cd "$APP_DIR"
    - make test
  needs:
    - Build the tester
  except:
    - master

Create release:
  stage: release
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  before_script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  needs:
    - Build the builder
  script:
    - cd "$APP_DIR"
    - export APP_NAME=`grep 'app:' mix.exs | sed -e 's/\[//g' -e 's/ //g' -e 's/app://' -e 's/[:,]//g'`
    - export APP_VSN=`grep 'version:' mix.exs | cut -d '"' -f2`
    - export BUILD="${CI_COMMIT_SHORT_SHA}"
    - export MIX_ENV='prod'
    - /kaniko/executor --context="${CI_PROJECT_DIR}"
      --cache
      --dockerfile="${CI_PROJECT_DIR}/Dockerfile"
      --target="production"
      --build-arg APP_NAME="${APP_NAME}"
      --build-arg APP_VSN="${APP_VSN}"
      --destination="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
      --destination="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
  except:
    - master

.tag-image: &tag-image
  stage: tag image
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  before_script:
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

Tag branch image:
  <<: *tag-image
  # We only tag the image if the tests passed
  needs:
    - Create release
    - Test
  script:
    - crane tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" "branch-${CI_COMMIT_REF_SLUG}"
  only:
    - branches
  except:
    - master

Tag image:
  <<: *tag-image
  needs:
    - Create release
  script:
    - crane tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" "${CI_COMMIT_TAG}"
  only:
    - tags

Tag image as stable:
  <<: *tag-image
  script:
    - crane tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" "stable"
    - crane tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" "stable-${CI_COMMIT_SHORT_SHA}"
  only:
    - master

Push to prod:
  stage: deploy to production
  before_script:
    - mkdir -p ~/.ssh && echo "$SSH_PRIVATE_KEY" |
      tr -d '\r' > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
    - cat "$SSH_HOST_FINGERPRINT" >> ~/.ssh/known_hosts
  script:
    - ssh restart-saturn@$SATURN_HOST
  only:
    - master
