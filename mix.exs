defmodule ProjectSaturn.MixProject do
  use Mix.Project

  def project do
    [
      app: :project_saturn,
      version: "0.2.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {ProjectSaturn.Application, []},
      extra_applications: [:logger, :runtime_tools, :set_locale]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:etso, "~> 0.1.1"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:phoenix, "~> 1.4.10"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_live_view, "~> 0.3.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_slime, "~> 0.12.0"},
      {:plug_cowboy, "~> 2.0"},
      {:plug_redirect, "~> 1.0"},
      {:bamboo, "~> 1.6"},
      {:basic_auth, "~> 2.2.2"},
      {:csv, "~> 2.3"},
      {:slugify, "~> 1.2"},
      {:earmark, "~> 1.4.13"},
      {:veil, "~> 0.2"},
      {:set_locale, "~> 0.2.1"},

      # Test
      # Needed by live view
      {:floki, ">= 0.0.0", only: :test},

      # Dev
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:mix_test_watch, "~> 0.8", only: :dev, runtime: false}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"]
    ]
  end
end
