// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import sass from "../css/app.sass"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"
import {Socket} from "phoenix"
import LiveSocket from "phoenix_live_view"

const hooks = {
  live_upload: {
    // This hook is executed each time a template with `phx-hook="live_upload"`
    // is rendered - @awea 20200314
    mounted(){
      // Since LiveView doesn't supports file uploads yet, we simply read the content of the file
      // using JavaScript then we paste it to an hidden textarea - @awea 20191113
      // Related issue: https://github.com/phoenixframework/phoenix_live_view/issues/104
      let $liveUploadInput = document.querySelector('#liveUpload')

      if ($liveUploadInput){
        let $liveUploadContent = document.getElementById($liveUploadInput.dataset.liveUploadContent)
        let $liveUploadFilename = document.getElementById($liveUploadInput.dataset.liveUploadFilename)

        $liveUploadInput.addEventListener('change', ({target: {files}}) => {
          let reader = new FileReader()

          reader.onload = ({target: {result}}) => {
            $liveUploadContent.value = result
          }

          // We assume there is only one file in this form - @awea 20191105
          reader.readAsText(files[0])

          // Group name are deduce using uploaded filename - @awea 20191113
          if ($liveUploadFilename){
            $liveUploadFilename.value = files[0].name
          }
        })
      }
    }
  }
}

let liveSocket = new LiveSocket("/live", Socket, { hooks })
liveSocket.connect()
