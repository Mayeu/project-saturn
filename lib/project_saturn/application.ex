defmodule ProjectSaturn.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      ProjectSaturn.Repo,
      # Start the endpoint when the application starts
      ProjectSaturnWeb.Endpoint,
      # Build ETS tables on startup, I haven't any better idea at the moment
      # on where to put this - @awea 20191111
      # SeedRepoFromFiles
      ProjectSaturnWeb.SeedRepoFromFiles
      # Starts a worker by calling: ProjectSaturn.Worker.start_link(arg)
      # {ProjectSaturn.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ProjectSaturn.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ProjectSaturnWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
