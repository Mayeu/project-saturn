defmodule ProjectSaturn.Veil.User do
  @moduledoc """
  Veil's User Schema
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias ProjectSaturn.Veil.{Request, Session}

  @primary_key {:id, :id, autogenerate: true}
  @required_fields [:name, :email, :group, :tags, :verified]

  schema "veil_users" do
    # Required fields
    field(:name, :string)
    field(:email, :string)
    field(:group, :string)
    field(:tags, :string)

    # Default for veil, not sure if it is actually needed
    field(:verified, :boolean, default: false)

    has_many(:requests, Request)
    has_many(:sessions, Session)

    timestamps()
  end

  # TODO 2020-02-23: This currently break Veil for an unknown reason
  def changeset(params \\ %{}) do
    changeset(%__MODULE__{}, params)
  end

  def changeset(model, params) do
    model
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> make_email_lowercase()
    |> unique_constraint(:email)
    |> group_slugify
  end

  defp make_email_lowercase(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{email: email}} ->
        put_change(changeset, :email, email |> String.downcase())

      _ ->
        changeset
    end
  end

  defp group_slugify(changeset) do
    update_change(changeset, :group, &Slug.slugify/1)
  end

  def groups(users) do
    users
    |> Enum.map(&Map.get(&1, :group))
  end
end
