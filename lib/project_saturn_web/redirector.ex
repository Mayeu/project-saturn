defmodule ProjectSaturnWeb.Redirector do
  use Plug.Redirect

  # See https://github.com/lpil/plug-redirect
  
  # TODO 2020-02-12 Mayeu: maybe replace with a simple plug like here:
  # https://elixirforum.com/t/redirecting-with-anon-fn-within-the-router/6614/2
  # instead of an external deps hidden in a dedicated file?

  redirect("/", "/login")
end
