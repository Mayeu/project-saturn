defmodule ProjectSaturnWeb.Router do
  use ProjectSaturnWeb, :router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(ProjectSaturnWeb.Plugs.Veil.UserId)
    plug(ProjectSaturnWeb.Plugs.Veil.User)
    plug(ProjectSaturnWeb.Plugs.SetLocale)
    plug Phoenix.LiveView.Flash
  end

  pipeline :admin do
    plug(:put_layout, {ProjectSaturnWeb.LayoutView, "admin.html"})
    plug BasicAuth, use_config: {:basic_auth, :admin_auth}
  end

  scope "/", ProjectSaturnWeb do
    pipe_through([:browser, ProjectSaturnWeb.Plugs.Veil.Authenticate])

    get "/", DashboardController, :index
  end

  scope "/login", ProjectSaturnWeb.Veil do
    pipe_through(:browser)

    get("/", UserController, :new)
  end

  scope "/admin", ProjectSaturnWeb do
    pipe_through(:browser)
    pipe_through(:admin)

    live "/", AdminLive.Home
    live "/users", AdminLive.Users
    live "/groups", AdminLive.Groups
  end

  # This plug allows you to view all delivered emails
  # Documentation: https://hexdocs.pm/bamboo/Bamboo.SentEmailViewerPlug.html
  if Mix.env() == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  # Default Routes for Veil
  scope "/veil", ProjectSaturnWeb.Veil do
    pipe_through(:browser)

    post("/users", UserController, :create)

    # get("/users/new", UserController, :new)
    get("/sessions/new/:request_id", SessionController, :create)
    get("/sessions/signout/:session_id", SessionController, :delete)
  end
end
