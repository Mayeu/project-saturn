defmodule ProjectSaturnWeb.GroupCreationService do
  alias ProjectSaturn.Repo
  alias ProjectSaturnWeb.Group

  def call(%{"filename" => filename, "markdown" => content}), do: call(filename, content)

  def call(md_name, md_content) do
    group_name = Path.basename(md_name, ".md")

    Group
    |> Repo.get_by(name: group_name)
    |> create_or_update_group(%{name: group_name, markdown_content: md_content})
    |> Repo.insert_or_update()
  end

  defp create_or_update_group(nil, changes), do: Group.changeset(changes)

  defp create_or_update_group(group, changes) do
    group
    |> Group.changeset(changes)
  end
end
