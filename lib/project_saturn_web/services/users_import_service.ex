defmodule ProjectSaturnWeb.UsersImportService do
  alias ProjectSaturn.Veil.User
  alias ProjectSaturn.Repo

  def call({:error, _reason}), do: :nothing
  def call({:ok, csv_content}), do: call(csv_content)

  def call(csv_content) do
    # Since LiveView doesn't supports file uploads yet, we
    # receive directly the content of the CSV so it needs
    # to be transformed in Stream before it can be used by the CSV hex package - @awea 20191105
    csv_content
    |> String.split("\n")
    |> Stream.map(& &1)
    |> CSV.decode(headers: true, validate_row_length: false, strip_fields: true)
    |> Enum.map(fn {_ok, line} -> line end)
    |> Enum.filter(&empty_line/1)
    |> Enum.map(&remove_mailto/1)
    |> Enum.map(&create_user/1)
  end

  defp create_user(%{"Name" => name, "email" => email, "Groupe" => group, "Tags" => tags}) do
    User
    |> Repo.get_by(email: email)
    |> create_or_update_user(%{name: name, email: email, group: group, tags: tags})
    |> Repo.insert_or_update()
  end

  defp create_or_update_user(nil, changes), do: User.changeset(%User{}, changes)

  defp create_or_update_user(user, changes) do
    user
    |> User.changeset(changes)
  end

  # For some reason an empty line is: %{"Name" => ""} - @awea 20191105
  # hashtag #lefun - @mayeu 20200219
  defp empty_line(%{"Name" => ""}), do: false
  defp empty_line(_), do: true

  defp remove_mailto(%{"email" => "mailto:" <> rest} = user), do: %{user | "email" => rest}

  defp remove_mailto(user), do: user
end
