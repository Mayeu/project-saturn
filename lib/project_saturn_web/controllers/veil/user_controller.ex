defmodule ProjectSaturnWeb.Veil.UserController do
  use ProjectSaturnWeb, :controller
  alias ProjectSaturn.Veil
  alias ProjectSaturn.Veil.User

  # action_fallback(ProjectSaturnWeb.Veil.FallbackController)

  plug(:scrub_params, "user" when action in [:create])

  @doc """
  Shows the sign in form
  """
  def new(conn, _params) do
    conn
    |> assign(:site_name, Application.get_env(:veil, :site_name))
    |> assign(:form_error, false)
    |> assign(:changeset, User.changeset())
    |> render("new.html", site_name: @site_name)
  end

  @doc """
  If needed, creates a new user, otherwise finds the existing one.
  Creates a new request and emails the unique id to the user.
  """
  def create(conn, %{"user" => %{"email" => email}}) do
    case Veil.get_user_by_email(email) do
      nil ->
        error_signin(conn)

      user ->
        sign_and_email(conn, user)
    end
  end

  defp error_signin(conn) do
    conn
    |> assign(:site_name, Application.get_env(:veil, :site_name))
    |> assign(:form_error, true)
    |> assign(:changeset, User.changeset())
    |> render("new.html", site_name: @site_name)
  end

  defp sign_and_email(conn, %User{} = user) do
    with {:ok, request} <- Veil.create_request(conn, user),
         email <- Veil.send_login_email(conn, user, request) do
      render(conn, "show.html", user: user, email: email, site_name: @site_name)
    else
      _ ->
        error_signin(conn)
    end
  end
end
