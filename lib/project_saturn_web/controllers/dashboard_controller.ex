defmodule ProjectSaturnWeb.DashboardController do
  use ProjectSaturnWeb, :controller

  alias ProjectSaturnWeb.Group
  alias ProjectSaturn.Veil.User
  alias ProjectSaturn.Repo

  def index(conn, _params) do
    user = get_user(conn)
    group = Group |> Repo.get_by(name: user.group)

    conn
    # This is a bit of repetition since we have the veil_user, but since it's
    # not store correctly in the session it's easier to have it duplicated.
    # @mayeu 2020-02-23
    |> assign(:user, user)
    |> assign(:group, group)
    |> render("index.html", site_name: @site_name)
  end

  defp get_user(conn) do
    # Totally unsure why there is this {:ok, %User{}} instead of just %User{}
    # in there. @mayeu 2020-02-23
    {:ok, user} = conn.assigns[:veil_user]
    user
  end
end
