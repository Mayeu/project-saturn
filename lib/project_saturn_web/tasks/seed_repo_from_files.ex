defmodule ProjectSaturnWeb.SeedRepoFromFiles do
  use Task

  def start_link(arg) do
    Task.start_link(__MODULE__, :run, [arg])
  end

  def run(_arg) do
    ProjectSaturnWeb.CSVFile.read()
    |> ProjectSaturnWeb.UsersImportService.call()

    ProjectSaturnWeb.MDFile.list()
    |> Enum.map(fn filename ->
      %{"filename" => filename, "markdown" => ProjectSaturnWeb.MDFile.read!(filename)}
    end)
    |> Enum.each(&ProjectSaturnWeb.GroupCreationService.call/1)
  end
end
