defmodule ProjectSaturnWeb.LoginLive.Index do
  use Phoenix.LiveView
  alias ProjectSaturnWeb.{LoginLive, LoginView, Mailer, LoginEmail}
  alias ProjectSaturn.Veil.User
  alias ProjectSaturn.Repo
  alias ProjectSaturnWeb.Router.Helpers, as: Routes

  def render(assigns) do
    ~L"""
    <%= Phoenix.View.render(LoginView, "login.html", assigns) %>
    """
  end

  def mount(_session, socket) do
    user = User.changeset()

    socket =
      socket
      |> assign(:user, user)
      |> assign(:login_success, false)

    {:ok, socket}
  end

  def handle_event("validate", %{"user" => params}, socket) do
    user =
      socket.assigns.user
      |> User.changeset(params)
      |> Map.put(:action, :update)

    {:noreply, assign(socket, user: user)}
  end

  def handle_event("save", %{"user" => %{"email" => email}}, socket) do
    User
    |> Repo.get_by(email: email)
    |> case do
      nil ->
        {:noreply, put_flash(socket, :error, "Nay!")}

      user ->
        user
        |> LoginEmail.user_creation()
        |> Mailer.deliver_later()

        {:noreply, assign(socket, login_success: true)}
    end
  end
end
