defmodule ProjectSaturnWeb.AdminLive.Home do
  use Phoenix.LiveView
  alias ProjectSaturnWeb.AdminView

  def render(assigns \\ %{}) do
    ~L"""
    <%= Phoenix.View.render(AdminView, "index.html", assigns) %>
    """
  end

  def mount(_session, socket) do
    {:ok, assign(socket, :current_tab, 0)}
  end

  def handle_event("current_tab", %{"tab-index" => tab_index}, socket) do
    tab_index = String.to_integer(tab_index)

    {:noreply, assign(socket, :current_tab, tab_index)}
  end
end
