defmodule ProjectSaturnWeb.AdminLive.Groups do
  use Phoenix.LiveView
  alias ProjectSaturn.Repo
  alias ProjectSaturnWeb.{AdminView, Group, MDFile, GroupCreationService}

  def render(assigns \\ %{}) do
    ~L"""
    <%= Phoenix.View.render(AdminView, "groups.html", assigns) %>
    """
  end

  def mount(_session, socket) do
    {:ok, assign(socket, groups: groups())}
  end

  def handle_event("save", %{"group_creation" => params}, socket) do
    MDFile.save(params)
    GroupCreationService.call(params)

    {:noreply, assign(socket, groups: groups())}
  end

  defp groups, do: Repo.all(Group)
end
