defmodule ProjectSaturnWeb.AdminLive.Users do
  use Phoenix.LiveView
  alias ProjectSaturnWeb.{AdminView, UsersImportService, CSVFile}
  alias ProjectSaturn.Veil.User
  alias ProjectSaturn.Repo

  def render(assigns \\ %{}) do
    ~L"""
    <%= Phoenix.View.render(AdminView, "users.html", assigns) %>
    """
  end

  def mount(_session, socket) do
    {:ok, assign(socket, users: users())}
  end

  def handle_event("save", %{"users_import" => params}, socket) do
    csv_content =
      params
      |> Map.get("csv")

    UsersImportService.call(csv_content)
    CSVFile.save(csv_content)

    {:noreply, assign(socket, users: users())}
  end

  defp users, do: Repo.all(User)
end
