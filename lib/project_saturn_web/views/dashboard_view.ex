defmodule ProjectSaturnWeb.DashboardView do
  use ProjectSaturnWeb, :view
  alias ProjectSaturnWeb.Group

  def markdown_to_html(%Group{markdown_content: markdown}), do: markdown_to_html(markdown)

  def markdown_to_html(markdown) do
    Earmark.as_html!(markdown, footnotes: true)
    |> Phoenix.HTML.raw()
  end
end
