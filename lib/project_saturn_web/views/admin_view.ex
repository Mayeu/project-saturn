defmodule ProjectSaturnWeb.AdminView do
  use ProjectSaturnWeb, :view

  def is_active(test), do: if(test, do: "is-active", else: "")
end
