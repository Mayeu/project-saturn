defmodule ProjectSaturnWeb.Group do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :id, autogenerate: true}
  @required_fields [:name, :markdown_content]

  schema "categories" do
    field(:name, :string)
    field(:markdown_content, :string)
  end

  def changeset(params \\ %{}) do
    changeset(%__MODULE__{}, params)
  end

  def changeset(model, params) do
    cast(model, params, @required_fields)
    |> validate_required(@required_fields)
  end
end
