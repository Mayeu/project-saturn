defmodule ProjectSaturnWeb.Veil.LoginEmail do
  use Bamboo.Phoenix, view: ProjectSaturnWeb.Veil.EmailView
  import Bamboo.Email

  @doc """
  Generates an email using the login template.
  """
  def generate(email, url) do
    site_name = Application.get_env(:veil, :site_name)

    new_email()
    |> to(email)
    |> from(from_email())
    |> subject("Bienvenue sur #{site_name}!")
    |> render("login.html", %{url: url, site_name: site_name})
  end

  defp from_email do
    {Application.get_env(:veil, :email_from_name),
     Application.get_env(:veil, :email_from_address)}
  end
end
