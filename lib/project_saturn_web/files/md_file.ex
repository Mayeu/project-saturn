defmodule ProjectSaturnWeb.MDFile do
  def list do
    path()
    |> File.ls!()
    |> Enum.filter(fn file -> Path.extname(file) == ".md" end)
  end

  def read!(filename) do
    path(filename)
    |> File.read!()
  end

  def save(%{"filename" => filename, "markdown" => content}), do: save(filename, content)

  def save(filename, content) do
    path(filename)
    |> path_available
    |> File.write!(content)
  end

  def path(filename) do
    path()
    |> Path.join(filename)
  end

  def path do
    Application.get_env(:project_saturn, :file_system)
    |> Keyword.get(:path)
  end

  defp path_available(path) do
    path
    |> Path.dirname()
    |> File.stat()
    |> create_directory(path)
  end

  defp create_directory({:ok, %File.Stat{type: :directory}}, path), do: path

  defp create_directory({:error, _posix}, path) do
    path
    |> Path.dirname()
    |> File.mkdir!()

    path
  end
end
