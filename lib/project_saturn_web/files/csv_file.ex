defmodule ProjectSaturnWeb.CSVFile do
  @filename "users.csv"

  def save(csv_content) do
    path()
    |> path_available
    |> File.write!(csv_content)
  end

  def read do
    path()
    |> File.read()
  end

  def path do
    Application.get_env(:project_saturn, :file_system)
    |> Keyword.get(:path)
    |> Path.join(@filename)
  end

  defp path_available(csv_path) do
    csv_path
    |> Path.dirname()
    |> File.stat()
    |> create_directory(csv_path)
  end

  defp create_directory({:ok, %File.Stat{type: :directory}}, csv_path), do: csv_path

  defp create_directory({:error, _posix}, csv_path) do
    csv_path
    |> Path.dirname()
    |> File.mkdir!()

    csv_path
  end
end
