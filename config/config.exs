# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :project_saturn,
  ecto_repos: [ProjectSaturn.Repo]

# Configures the endpoint
config :project_saturn, ProjectSaturnWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "21dLNATBLYuEewVGf4+huX86Lki9NSS7pAK3uY0dBk45Bq671eaHNc+r7zkHvUmQ",
  render_errors: [view: ProjectSaturnWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ProjectSaturn.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "SECRET_SALT"
  ]

# Configure Phoenix template engine
config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine,
  slimleex: PhoenixSlime.LiveViewEngine

# Configure Gettext
config :project_saturn, ProjectSaturnWeb.Gettext, default_locale: "fr", locales: ~w(fr en)

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :gettext, :default_locale, "fr_FR"

# Configure Mailer
config :project_saturn, ProjectSaturnWeb.Mailer, adapter: Bamboo.MailgunAdapter

# -- Veil Configuration    Don't remove this line
config :veil,
  # How long should emailed sign-in links be valid for?
  sign_in_link_expiry: 12 * 3_600,
  # How long should sessions be valid for?
  session_expiry: 86_400 * 30,
  # How often should existing sessions be extended to session_expiry
  refresh_expiry_interval: 86_400,
  # How many recent sessions to keep in cache (to reduce database operations)
  sessions_cache_limit: 250,
  # How many recent users to keep in cache
  users_cache_limit: 100

config :veil, Veil.Scheduler,
  jobs: [
    # Runs every midnight to delete all expired requests and sessions
    {"@daily", {ProjectSaturn.Veil.Clean, :expired, []}}
  ]

# -- End Veil Configuration

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
