# Project Saturn

## Run the project


    $ docker run -e ERLANG_COOKIE="super-secret-value" \
                 -e NODE_NAME="project-saturn-01"      \
                 -e SECRET_KEY_BASE="64bits-secret"    \
                 -e URL_HOST=localhost                 \
                 -v /path/to/uploads:/opt/app/uploads  \
                 -p 4000:4000                          \
                 registry.gitlab.com/mayeu/project-saturn:latest


## Additional Docker Images

The project CI automatically build and publish various image for each steps of
the build.

The canonical list [can be found in the GitLab Registry][gitlab-registry].

The `stable` tag contains the last production image, there is also a
`stable-<commit sha>` label to get the production image of a specific commit.

[gitlab-registry]: https://gitlab.com/Mayeu/project-saturn/container_registry
