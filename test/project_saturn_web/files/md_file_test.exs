defmodule ProjectSaturnWeb.MDFileTest do
  use ExUnit.Case
  alias ProjectSaturnWeb.MDFile

  setup do
    md_name = "group-bar.csv"
    md_content = File.read!("test/fixtures/group-bar.md")

    {:ok, md_content: md_content, md_name: md_name}
  end

  test "path returns path of given MD filename" do
    # TODO: The upload path is given via the environment. It should not be hardcoded
    assert MDFile.path("group-bar.md") == "/tmp/group-bar.md"
  end

  test "save will create MD file", %{md_content: md_content, md_name: md_name} do
    assert MDFile.save(md_name, md_content)
  end
end
