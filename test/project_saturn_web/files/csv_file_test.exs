defmodule ProjectSaturnWeb.CSVFileTest do
  use ExUnit.Case
  alias ProjectSaturnWeb.CSVFile

  setup do
    csv_content = File.read!("test/fixtures/users.csv")

    {:ok, csv_content: csv_content}
  end

  test "path returns path of CSV file" do
    # TODO: The upload path is given via the environment. It should not be hardcoded
    assert CSVFile.path() == "/tmp/users.csv"
  end

  test "save will create CSV file", %{csv_content: csv_content} do
    assert CSVFile.save(csv_content)
  end

  describe "for an already saved CSV file read" do
    setup %{csv_content: csv_content} do
      CSVFile.save(csv_content)

      {:ok, csv_content: csv_content}
    end

    test "will return content of CSV file", %{csv_content: csv_content} do
      assert CSVFile.read() == {:ok, csv_content}
    end
  end

  describe "for an unexisting CSV File read" do
    setup %{csv_content: csv_content} do
      CSVFile.path()
      |> File.rm()

      {:ok, csv_content: csv_content}
    end

    test "will return an error :enoent" do
      assert CSVFile.read() == {:error, :enoent}
    end
  end
end
