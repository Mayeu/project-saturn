defmodule ProjectSaturnWeb.Plugs.SetLocaleTest do
  use ProjectSaturnWeb.ConnCase
  alias ProjectSaturnWeb.Plugs.SetLocale

  test "conn status is ok" do
    conn = 
      conn
      |> SetLocale.call(%{})
      |> get("/login")

    assert conn.status == 200
  end

  describe "locale determined using accept-language" do
    test "default locale is fr" do
      assert get_locale_current_backend(conn, "martian") == "fr"
    end

    test "locale is en" do
      assert get_locale_current_backend(conn, "en-US") == "en"
    end

    test "locale is fr" do
      assert get_locale_current_backend(conn, "fr-Fr") == "fr"
    end
  end

  defp get_locale_current_backend(conn, accept_language) do
    conn =
        conn
        |> put_req_header("accept-language", accept_language)
        |> SetLocale.call(%{})
        |> get("/login")

    Gettext.get_locale(ProjectSaturnWeb.Gettext)
  end
end
