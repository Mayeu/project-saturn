defmodule ProjectSaturnWeb.GroupCreationServiceTest do
  use ExUnit.Case
  alias ProjectSaturnWeb.{GroupCreationService, Group}
  alias ProjectSaturn.Repo

  setup do
    md_name = "group-bar.md"
    md_content = File.read!("test/fixtures/group-bar.md")

    {:ok, md_content: md_content, md_name: md_name}
  end

  test "call with group-bar.md will creates 1 group", %{md_content: md_content, md_name: md_name} do
    GroupCreationService.call(md_name, md_content)
    groups = Repo.all(Group)
    group = List.first(groups)

    assert Enum.count(groups) == 1
    assert group.name == "group-bar"
  end

  test "call twice with group-bar.md will creates 1 group", %{
    md_content: md_content,
    md_name: md_name
  } do
    GroupCreationService.call(md_name, md_content)
    GroupCreationService.call(md_name, md_content)

    groups = Repo.all(Group)
    group = List.first(groups)

    assert Enum.count(groups) == 1
    assert group.name == "group-bar"
  end
end
