defmodule ProjectSaturnWeb.UsersImportServiceTest do
  use ExUnit.Case
  alias ProjectSaturnWeb.UsersImportService
  alias ProjectSaturn.Veil.User
  alias ProjectSaturn.Repo

  setup do
    csv_content = File.read!("test/fixtures/users.csv")

    {:ok, csv_content: csv_content}
  end

  test "call with users.csv creates 3 users", %{csv_content: csv_content} do
    UsersImportService.call(csv_content)
    users = Repo.all(User)

    assert Enum.count(users) == 3
  end
end
