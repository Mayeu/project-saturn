defmodule ProjectSaturnWeb.GroupTest do
  use ExUnit.Case
  alias ProjectSaturnWeb.Group

  test "changeset with valid attributes" do
    changeset = Group.changeset(%{name: "bar", markdown_content: "# Hi there"})
    assert changeset.valid?
  end

  describe "changeset with invalid attributes" do
    test "with empty params" do
      changeset = Group.changeset(%{})
      refute changeset.valid?
    end

    test "with empty markdown_content" do
      changeset = Group.changeset(%{name: "foo"})
      refute changeset.valid?
    end
  end
end
