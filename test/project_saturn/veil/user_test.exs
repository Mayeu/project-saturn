defmodule ProjectSaturn.Veil.UserTest do
  use ExUnit.Case
  alias ProjectSaturn.Veil.User

  test "changeset with valid attributes" do
    changeset = User.changeset(%{name: "bar", email: "foo@foo.com", group: "foo", tags: "a,b"})

    assert changeset.valid?
  end

  test "group will be sluggified" do
    changeset = User.changeset(%{name: "bar", email: "foo@foo.com", group: "BAR bob", tags: "a"})

    assert changeset.changes.group == "bar-bob"
  end

  describe "changeset with invalid attributes" do
    test "invalid email" do
      changeset = User.changeset(%{email: "foo"})
      refute changeset.valid?
    end

    test "empty params" do
      changeset = User.changeset()
      refute changeset.valid?
    end
  end

  describe "for users with group" do
    setup do
      users = [%User{group: "foo"}, %User{group: "bar"}]

      {:ok, users: users}
    end

    test "groups return [foo, bar]", %{users: users} do
      assert User.groups(users) == ["foo", "bar"]
    end
  end
end
